<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pdv extends Model
{

    use SoftDeletes;//
    protected $guarded = ['id'];
     
    protected $dates = ['deleted_at'];

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }

}
