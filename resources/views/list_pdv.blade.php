<!doctype html>
<html>
<head>
    @include('head')
</head>
<div class="conteiner">

    <div id="main" class="row">
        <h1>Pdv List</h1> <a href="/pdvs/create">New Pdv</a>
        <table>
          <tr>
            <td>Id</td>
            <td>Trading Name</td>
            <td>Owner Name</td>
            <td>Document</td>
            <td>CoverageArea Coordinates</td>
            <td>Address Lat</td>
            <td>Address Lng</td>

          </tr>
          @foreach ($pdvs as $pdv)
              <tr>
                <td>{{ $pdv->id }}</td>
                <td>{{ $pdv->tradingName }}</td>
                <td>{{ $pdv->ownerName }}</td>
                <td>{{ $pdv->document }}</td>
                <td>{{ $pdv->coverageArea_coordinates }}</td>
                <td>{{ $pdv->address_lat }}</td>
                <td>{{ $pdv->address_lng }}</td>
                <td><a href="/pdvs/{{ $pdv->id }}">Show</a> -
                <td><a href="/pdvs/{{ $pdv->id }}/edit">Edit</a> -
                <a href="/pdvs/delete_pdv/{{ $pdv->id }}">Delete</a></td>
              </tr>
          @endforeach
        </table>
    </div>
</div>
</body>
</html>