<?php

namespace App\Http\Controllers;

use App\Pdv as Pdv;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PdvController extends Controller
{

    public function index()
    {
        $pdvs = Pdv::all();
        
        return view('list_pdv', [
            "pdvs" => $pdvs
        ]);
    }

    public function create()
    {
        $pdvs = Pdv::all();
        return view('create_pdv', [
            "pdvs" => $pdvs
        ]);
    }

    public function store(Request $request)
    {
        $pdv = Pdv::create([
            'tradingName' => $request->tradingName,
            'ownerName' => $request->ownerName,
            'document' => $request->document,
            'coverageArea_coordinates' => $request->coverageArea_coordinates,
            'address_lat' => $request->address_lat,
            'address_lng' => $request->address_lng
        ]);

        $pdvs = Pdv::all();
        return view('list_pdv', [
            "pdvs" => $pdvs
        ]);
    }

    public function show(Pdv $pdv)
    {
        $pdv->coverageArea = array(
            'type' => 'MultiPolygon', 
            'coordinates' => $pdv->coverageArea_coordinates
        );
        $pdv->address = array(
            'type' => 'Point', 
            'coordinates' => '['.$pdv->address_lat.', '.$pdv->address_lng.']'
        );

        unset($pdv->coverageArea_coordinates, $pdv->address_lat, $pdv->address_lng, 
            $pdv->deleted_at, $pdv->created_at, $pdv->updated_at);
        return $pdv;
    }

    public function edit(Pdv $pdv)
    {
        return view('edit_pdv', [
            "pdv" => $pdv
        ]);
    }

    // Retorna se a localizacao pertence a uma area de pdv
    public function pointInPolygon($lat, $lng, $poly)
    {
        $j=count($poly)-1;
        $oddNodes=0;
        $polyX = $polyY = array();

        for ($i=0; $i<count($poly); $i++)
        {
            if ($poly[$i][1]<$lng && $poly[$j][1]>=$lng || $poly[$j][1]<$lng && $poly[$i][1]>=$lng)
            {
                if ($poly[$i][0]+($lng-$poly[$i][1])/($poly[$j][1]-$poly[$i][1])*($poly[$j][0]-$poly[$i][0])<$lat)
                {
                    $oddNodes = ($oddNodes+1)%2; 
                }
            }
            $j = $i;
        }

        return $oddNodes;
    }


    // Retorna a distancia das coordenadas com os Pdvs 
    public function pdvDist($lat, $lng, Pdv $pdv)
    {
        $a = $pdv->address_lat-$lat;
        $b = $pdv->address_lng-$lng;
        return sqrt($a*$a + $b*$b);
    }

    // Retorna o PDV mais perto que atenda a area de cobertura
    public function closer_pdv($lat = 0, $lng = 0, Pdv $pdv)
    {
        if($lat == 0 && $lng == 0) 
            return "Adicionar Latitude|Longitude no final da URL";
        $minDist = -1;
        $closerPdv = null;
        $pdvs = Pdv::all();

        foreach($pdvs as $pdv)
        {
            $polys = json_decode($pdv->coverageArea_coordinates, true);
            for($x = 0; $x < count($polys); $x++)
            {
                if($this->pointInPolygon($lat, $lng, $polys[$x]))
                {
                    $dist = $this->pdvDist($lat, $lng, $pdv);
                    if($dist < $minDist || $minDist < 0)
                    {
                        $minDist = $dist;
                        $closerPdv = $pdv;
                    }
                }
            }
        }
        if(!$closerPdv) 
        {
            return "Sem Pdv cadastrado para atender essas coordenadas!";
        }
        return $this->show($closerPdv);
    }

    public function update(Request $request, Pdv $pdv)
    {
        $pdv->tradingName = $request->tradingName;
        $pdv->ownerName = $request->ownerName;
        $pdv->save();

        $pdvs = Pdv::all();
        return view('list_pdv', [
            "pdvs" => $pdvs
        ]);
    }

    public function delete_pdv(Pdv $pdv) {
        return view('delete_pdv', [
            "pdv" => $pdv
        ]);
    }

    public function destroy(Pdv $pdv)
    {
        $pdv->delete();

        $pdvs = Pdv::all();
        return view('list_pdv', [
            "pdvs" => $pdvs
        ]);
    }
}
