<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Edit PDV</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            td {
              border: 1px solid black;
              margin: 0;
            }
        </style>
    </head>
    <body>
        <h1>Editing PDV {{ $pdv->name }}</h1>
        <form action="/pdvs/{{ $pdv->id }}" method="POST">
            {{ method_field('PUT') }}
            {{ csrf_field() }}
            Trading Name: <input type="text" name="tradingName" value="{{ $pdv->tradingName }}"> <br />
            Owner Name: <input type="text" name="ownerName" value="{{ $pdv->ownerName }}"> <br />
            <input type="submit" value="Submit!" />
        </form>
    </body>
</html>
