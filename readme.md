Esse projeto é uma resolução do problema proposto:

https://github.com/ZXVentures/code-challenge/blob/master/backend.md

Para execução será necessário:

1- Ter Laravel e Mysql preparados na Máquina

2- Clonar o Repositório

3- Acessar a pasta e executar o comando: php artisan serve

4- Acessar http://127.0.0.1:8000/pdvs

Tarefas do Desafio:

1- Acessar http://127.0.0.1:8000/pdvs/create
2- Acessar http://127.0.0.1:8000/pdvs/1
3- Acessar http://127.0.0.1:8000/pdvs/closer_pdv

Sugestão: Crie primeiro PDV a partir do Exemplo do Desafio:

{
  "pdvs": [ 
    {
        "id": 1, 
        "tradingName": "Adega da Cerveja - Pinheiros",
        "ownerName": "Zé da Silva",
        "document": "1432132123891/0001", //CNPJ
        "coverageArea": { 
          "type": "MultiPolygon", 
          "coordinates": [
            [[[30, 20], [45, 40], [10, 40], [30, 20]]], 
            [[[15, 5], [40, 10], [10, 20], [5, 10], [15, 5]]]
          ]
        }, //Área de Cobertura
        "address": { 
          "type": "Point",
          "coordinates": [-46.57421, -21.785741]
        }, // Localização do PDV
    }
  ]
}