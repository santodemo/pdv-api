<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
	    DB::table('projects')->insert([
            'name' => str_random(10),
            'description' => str_random(100),
            'num_contributors' => rand(0,100),
            'num_contributions' => rand(0,100),
            'followers' => rand(0,100),
            'creator_id' => rand(0,100)

        ]);

        // DB::statement('SET FOREIGN_KEY_CHECKS = 0'); Can be used to avoid foreign key check
        DB::table('tasks')->insert([
            'project_id' => 1,
            'name' => str_random(10),
            'description' => str_random(100)
        ]);

    }
}
