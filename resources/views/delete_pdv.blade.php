<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Delete PDV</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            td {
              border: 1px solid black;
              margin: 0;
            }
        </style>
    </head>
    <body>
        <h1>Do you really want to delete {{ $pdv->tradingName }} PDV?</h1>
        <form action="/pdvs/{{ $pdv->id }}" method="POST">
            {{ method_field('DELETE') }}
            {{ csrf_field() }}
            <input type="submit" value="For sure!" />
        </form>
    </body>
</html>
