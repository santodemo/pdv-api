<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Create PDV</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            td {
              border: 1px solid black;
              margin: 0;
            }
        </style>
    </head>
    <body>
        <h1>Create Pdvs</h1>
        <form action="/pdvs" method="POST">
            {{ method_field('POST') }}
            {{ csrf_field() }}
            TradingName: <input type="text" name="tradingName"> <br />
            OwnerName: <input type="text" name="ownerName"> <br />
            Document: <input type="text" name="document"> <br />
            CoverageAreaCoordinates: <input type="text" name="coverageArea_coordinates"> <br />
            AddressLat: <input type="int" name="address_lat"> <br />
            AddressLng: <input type="int" name="address_lng"> <br />
            <input type="submit" value="Submit!" />
        </form>
    </body>
</html>
